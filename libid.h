#ifndef MOST_SIMPLE_ID_CHEKER
#define MOST_SIMPLE_ID_CHEKER

#include <iostream>
#include <string>

int CheckIfId(const std::string &ID){
  unsigned int SUM=0;
  unsigned short int value=10;
  unsigned int result = SUM % 11;
  for(size_t i=0;i<ID.length() -1 ;i++){
      SUM += ((int)ID.at(i) - 48) * value;
      value--;
    }
  if (result > 2 && ((int)ID.at(9) - 48) == result){
      return 0;
    }
  else if (((int)ID.at(9) - 48) == (11 - result)){
      return 0;
    }
  else{
      return -1;
    }
  return -3;
}

#endif // MOST_SIMPLE_ID_CHEKER
